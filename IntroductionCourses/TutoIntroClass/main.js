

// Class Ball
class Ball
{
    constructor()
    {
        this.x=100;
        this.y=100;
        this.vx=1;
        this.vy=1;
        this.radius=50;
        this.speed=5;
    }
}


const FPS = 60;

var ball = new Ball();

function setup() 
{

createCanvas(600, 400);
frameRate(FPS); // Appel mon Draw toutes FPS

}

function Update()
{
    ball.x+=ball.vx*ball.speed;
    ball.y+=ball.vy*ball.speed;
    if ( (ball.x>=600) && (ball.vx>0))
    ball.vx = -1;
    if ( (ball.x<=0) && (ball.vx<0))
    ball.vx = 1;
    if ( (ball.y>=400) && (ball.vy>0))
    ball.vy = -1;
    if ( (ball.y<=0) && (ball.vy<0))
    ball.vy = 1;
}

function Draw()
{
    circle(ball.x, ball.y, ball.radius);
}

function draw() 
{
background(220);

    Update();
    Draw();

}